# Run this program on your local python
# interpreter, provided you have installed
# the required libraries.

# Importing the required packages
import charset_normalizer
import numpy as np
import seaborn as sn
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn import tree
import pydotplus
import matplotlib.pyplot as plt
import matplotlib.image as pltimg



# Function importing Dataset
def importdata():
    balance_data = pd.read_csv("new_dataset.csv", sep=',', header=None)

    # Printing the dataswet shape
    print("Dataset Length: ", len(balance_data))
    print("Dataset Shape: ", balance_data.shape)

    # Printing the dataset obseravtions
    print("Dataset: ", balance_data.head())
    return balance_data


# Function to split the dataset
def splitdataset(balance_data):
    # Separating the target variable
    X = balance_data.values[1:, 2:11]
    Y = balance_data.values[1:, 11]

    # Splitting the dataset into train and test
    X_train, X_test, y_train, y_test = train_test_split(
        X, Y, test_size=0.3, random_state=20)

    return X, Y, X_train, X_test, y_train, y_test


# Function to perform training with giniIndex.
def train_using_gini(X_train, X_test, y_train):
    # Creating the classifier object
    clf_gini = DecisionTreeClassifier(criterion="gini",
                                      random_state=100, max_depth=7, min_samples_leaf=5)

    # Performing training
    clf_gini.fit(X_train, y_train)
    return clf_gini


# Function to perform training with entropy.
def tarin_using_entropy(X_train, X_test, y_train):
    # Decision tree with entropy
    clf_entropy = DecisionTreeClassifier(
        criterion="entropy", random_state=100,
        max_depth=7, min_samples_leaf=5)

    # Performing training
    clf_entropy.fit(X_train, y_train)
    features = ['distance_to_me','distance_to_road','vehicle_annotation','is_Looking','is_Crossing','distance_to_cars','xbl','ybl','xbr']
    data = tree.export_graphviz(clf_entropy, out_file=None, feature_names=features)
    graph = pydotplus.graph_from_dot_data(data)
    graph.write_png('mydecisiontree.png')

    img = pltimg.imread('mydecisiontree.png')
    imgplot = plt.imshow(img)
    plt.show()
    return clf_entropy


# Function to make predictions
def prediction(X_test, clf_object):
    # Predicton on test with giniIndex
    y_pred = clf_object.predict(X_test)
    print("Predicted values:")
    print(y_pred)
    return y_pred


# Function to calculate accuracy
def cal_accuracy(y_test, y_pred):
    print("Confusion Matrix:\n",
          confusion_matrix(y_test, y_pred))
    cm = confusion_matrix(y_test, y_pred)
    cmn = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    fig, ax = plt.subplots(figsize=(10, 10))
    sn.set(font_scale=1.4)
    sn.heatmap(cmn, annot=True, fmt='.2f', xticklabels= [i for i in "123"], yticklabels= [i for i in "123"], cmap='Blues', )
    plt.ylabel('Actual')
    plt.xlabel('Predicted')
    plt.show(block=False)
    print("Accuracy : ",
          accuracy_score(y_test, y_pred) * 100)

    print("Report : ",
          classification_report(y_test, y_pred))


# Driver code
def main():
    # Building Phase
    data = importdata()
    X, Y, X_train, X_test, y_train, y_test = splitdataset(data)
    clf_gini = train_using_gini(X_train, X_test, y_train)
    clf_entropy = tarin_using_entropy(X_train, X_test, y_train)

    # Operational Phase
    print("Results Using Gini Index:")

    # Prediction using gini
    y_pred_gini = prediction(X_train, clf_gini)
    cal_accuracy(y_train, y_pred_gini)

    print("Results Using Entropy:")
    # Prediction using entropy
    y_pred_entropy = prediction(X_train, clf_entropy)
    cal_accuracy(y_train, y_pred_entropy)

    import pickle
    s = pickle.dumps(clf_gini)
    with open('DT_clf_gini.pkl', 'wb') as fid:
        pickle.dump(clf_gini, fid)

# Calling main function
if __name__ == "__main__":
    main()