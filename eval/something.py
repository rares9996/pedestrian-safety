import cv2
import os

# image_folder = 'C:/Users/Rares/Desktop/Licenta/20220513_2048'
# video_name = 'video2.mp4'
#
# images = [img for img in os.listdir(image_folder) if img.endswith(".jpg")]
# frame = cv2.imread(os.path.join(image_folder, images[0]))
# height, width, layers = frame.shape
#
# video = cv2.VideoWriter(video_name, 0, 24, (width,height))
#
# for image in images:
#     video.write(cv2.imread(os.path.join(image_folder, image)))
#
# cv2.destroyAllWindows()
# video.release()
import re
import xml.etree.cElementTree as ET

def main():
    label_folder = 'C:/Users/Rares/Desktop/Licenta/20220606_1644'
    labels = [label for label in os.listdir(label_folder) if label.endswith(".txt")]
    action = "stopped"
    root = ET.Element("vehicle_info")
    for index, label in enumerate(labels):

        with open(label_folder+"/"+label) as f:
            lines = f.readlines()
            result = re.search("N,(.*),K", lines[0])
            if(result == None):
                action = 'stopped'
            else:
                speed = round(float(result.group(1)))
                if(speed < 35):
                    action = "moving_slow"
                else:
                    action = "moving_fast"
        ET.SubElement(root, "frame", action=action, id=str(index))
    tree = ET.ElementTree(root)
    tree.write("video3.xml")


if __name__ == "__main__":
        main()