import torch
import os

from PIL import Image
from argparse import ArgumentParser
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, Resize
from torchvision.transforms import ToTensor

from my_dataset_video import cityscapes
from eval.eval_segmentation.erfnet import ERFNet
from my_results import Results

NUM_CHANNELS = 3
NUM_CLASSES = 20

input_transform_cityscapes = Compose([
    Resize((640, 640), Image.BILINEAR),
    ToTensor(),
])


def load_my_state_dict(model, state_dict):
    own_state = model.state_dict()
    for name, param in state_dict.items():
        if name not in own_state:
            continue
        own_state[name].copy_(param)
    return model
def main(args):
    if not args.yolop:
        modelpath = args.loadDir + args.loadModel
        weightspath = args.loadDir + args.loadWeights

        print("Loading model: " + modelpath)
        print("Loading weights: " + weightspath)

        model = ERFNet(NUM_CLASSES)

        model = torch.nn.DataParallel(model)
        if (not args.cpu):
            model = model.cuda()

        model = load_my_state_dict(model, torch.load(weightspath))
        print("Model and weights LOADED successfully")

        model.eval()

        if (not os.path.exists(args.datadir)):
            print("Error: datadir could not be loaded")
        if (not os.path.exists(args.annotations)):
            print("Error: annotations could not be loaded")
    else:
        model = torch.hub.load('hustvl/yolop', 'yolop', pretrained=True)
        model = model.cuda()
    loader = DataLoader(
        cityscapes(args.datadir, input_transform_cityscapes),
        num_workers=args.num_workers, batch_size=args.batch_size, shuffle=False)
    Results().calculate(model, loader, args.annotationsVehicle,args.annotations, args.yolop,args.model_trained)

if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('--state')

    parser.add_argument('--loadDir', default="../trained_models/")
    parser.add_argument('--loadWeights', default="erfnet_pretrained.pth")
    parser.add_argument('--loadModel', default="erfnet.py")

    parser.add_argument('--datadir', default="video2.mp4")
    parser.add_argument('--annotations', default="D:\DATASET\JAAD_clips\\annotations\\video_2.xml")
    parser.add_argument('--annotationsVehicle', default="filename.xml")
    parser.add_argument('--num-workers', type=int, default=4)
    parser.add_argument('--batch-size', type=int, default=1)
    parser.add_argument('--cpu', action='store_true')
    parser.add_argument('--yolop', type=bool, default=True )
    parser.add_argument('--model_trained', type=int, default=1)

    parser.add_argument('--visualize', type=bool, default=0)
    main(parser.parse_args())
