import random

import pandas as pd


def importdata(data_array):
    # balance_data = pd.read_csv("C:\\Users\\Rares\\Desktop\\Book1.csv", sep=',', header=None)
    datas = []
    # for data_array in balance_data.values:
    isLooking = data_array[6]
    isCrossing = data_array[7]
    for index, data in enumerate(data_array):
        if data == -1:
            data = random.randint(800,1300)
        if data == 'b':
            data = 2
        if  data == 'm':
            data = 1
            if(not isLooking and isCrossing):
                data = 2
        if  data == 'l':
            data = 0
            if(not isLooking or isCrossing):
                data = 1
        if data == "stopped":
            data = 0
        if data == "moving_slow":
            data = 1
        if data == "decelerating":
            data = 2
        if data == "accelerating":
            data = 3
        if data == "moving_fast":
            data = 4
        if data == True:
            data = 1
        if data == False:
            data = 0
        if(index == len(data_array)-1):
            datas.append(data)
        else:
            datas.append(data)
    return datas

if __name__ == "__main__":
    importdata()
