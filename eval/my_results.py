import csv
import time

import torch
import cv2
import numpy as np
import xml.etree.ElementTree as ET
import pickle
from random import randint
import math

from sklearn.metrics import plot_confusion_matrix
from torch.autograd import Variable
from my_transform import Colorize
from tracking.centroidtracker import CentroidTracker


class Results:
    def __init__(self):
        self.meanDistanceFromMe = {}
        self.meanDistanceFromCar = {}
        self.meanDistanceFromRoad = {}
        pass

    def read_from_xml(self, video_annotations, frame):
        with open(video_annotations, 'r') as file:
            annotations = file.read().rstrip()
        xml = ET.fromstring(annotations)
        frame = xml.findall('./frame')[frame]
        return frame.attrib['action']

    def read_from_xml_pedestrian(self, video_annotations, step, bbox):
        with open(video_annotations, 'r') as file:
            annotations = file.read().rstrip()
        xml = ET.fromstring(annotations)
        frame = xml.findall('./track')
        isLooking = True
        isCrossing = False
        min_distance = 9999
        found = 0
        for fr in frame:
            if fr.attrib['label'] == "pedestrian":
                if (len(fr.findall('./box')) <= step):
                    if found == 0:
                        found = False
                    continue
                box = fr.findall('./box')[step]
                (x2, y2, x1, y1) = (box.attrib['xbr'], box.attrib['ybr'], box.attrib['xtl'], box.attrib['ytl'])
                x2 = int(float(x2))
                y2 = int(float(y2))
                x1 = int(float(x1))
                y1 = int(float(y1))
                if self.doOverlap((x1, y1), (x2, y2), (bbox[0], bbox[1]), (bbox[2], bbox[3])):
                    dst = math.dist((x1 + x2 / 2, y1 + y2 / 2), (bbox[0] + bbox[2] / 2, bbox[1] + bbox[3] / 2))
                    if dst < min_distance:
                        min_distance = dst
                        for attrib in box.findall('./attribute'):
                            if  attrib.attrib.__contains__('look'):
                                isLooking = attrib.attrib['look'] == "looking"
                            if  attrib.attrib.__contains__('cross'):
                                isCrossing = attrib.attrib['cross'] == "crossing"
                            found = True
        return isLooking, isCrossing


    def click_event(self,event, x, y, flags, params):
        if event == cv2.EVENT_LBUTTONDOWN:
            print(x, ' ', y)
    @staticmethod
    def doOverlap(l1, r1, l2, r2):
        if (l1[0] >= r2[0]) or (r1[0] <= l2[0]) or (r1[1] <= l2[1]) or (l1[1] >= r2[1]):
            return False
        return True

    def calculate(self, model, loader, vehicle_annotation, ped_annotation, yolop, model_trained):
        import numpy as np
        ct = CentroidTracker()
        img_array=[]
        lowCorrect = 0
        mediumCorrect = 0
        highCorrect = 0
        lowMedium = 0
        highMedium=0
        mediumLow=0
        highLow=0
        lowHigh=0
        mediumHigh=0
        if (model_trained == 0):
            with open('../train/trained_models/DT_clf_gini.pkl', 'rb') as fid:
                clf_loaded = pickle.load(fid)
        if (model_trained == 1):
            with open('../train/trained_models/DT_rft.pkl', 'rb') as fid:
                clf_loaded = pickle.load(fid)
        if (model_trained == 2):
            with open('../train/trained_models/DT_adb.pkl', 'rb') as fid:
                clf_loaded = pickle.load(fid)
        if (model_trained == 3):
            with open('../train/trained_models/DT_svm.pkl', 'rb') as fid:
                clf_loaded = pickle.load(fid)
        for step, (images, numpyImage, height, width) in enumerate(loader):
            ped_looking_history = []
            ped_crossing_history = []
            images = images.cuda()
            if not yolop:
                inputs = Variable(images)
                with torch.no_grad():
                    outputs = model(inputs)
                label = outputs[0].max(0)[1].byte().cpu().data
            else:
                # vis = visdom.Visdom();
                # # vis.image()
                _, da_seg_out, _ = model(images)
                label = da_seg_out[0].max(0)[1].byte().cpu().data

            image = numpyImage[0].numpy()
            distance, image_road_contour,car_boxes = Colorize()(label.unsqueeze(0), step, height, width, image, yolop)
            distance = torch.from_numpy(np.array(distance))
            distance = distance.numpy()

            bboxes = []
            for index in range(int(len(distance[0]) / 4)):
                (x, y, w, h) = distance[3][index * 4:index * 4 + 4]
                x = int(x)
                y = int(y)
                w = int(w)
                h = int(h)
                bboxes.append((int(x), int(y), int(x + w), int(y + h), distance[0][index * 4],
                               distance[1][index * 4], distance[2][index * 4]))

            for i, newbox in enumerate(bboxes):
                p1 = (newbox[0], newbox[1])
                p2 = (newbox[2], newbox[3])
                cv2.rectangle(image_road_contour, p1, p2, (randint(0, 255), randint(0, 255), randint(0, 255)), 2, 1)
            for i, newbox in enumerate(car_boxes):
                p1 = (int(newbox[0]), int(newbox[1]))
                p2 = (int(newbox[0]+newbox[2]), int(newbox[1]+newbox[3]))
                cv2.rectangle(image_road_contour, p1, p2, (randint(0, 255), randint(0, 255), randint(0, 255)), 2, 1)

            objects = ct.update(bboxes)
            maxDangerGini = 1
            for (objectID, [centroid, distances, rect]) in objects.items():
                text = "ID {}".format(objectID)
                cv2.putText(image_road_contour, text, (centroid[0] - 10, centroid[1] - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.circle(image_road_contour, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
                if (not objectID in self.meanDistanceFromMe):
                    self.meanDistanceFromMe[objectID] = []
                    self.meanDistanceFromRoad[objectID] = []
                    self.meanDistanceFromCar[objectID] = []
                fromMe = self.calculateDistanceBetweenFrames(self.meanDistanceFromMe[objectID], distances[0])
                fromCar = self.calculateDistanceBetweenFrames(self.meanDistanceFromCar[objectID], distances[2])
                fromRoad = self.calculateDistanceBetweenFrames(self.meanDistanceFromRoad[objectID], distances[1])
                pedActrions = self.read_from_xml_pedestrian(ped_annotation, step, rect)
                if len(ped_looking_history) == 5:
                    ped_looking_history.pop()
                    ped_crossing_history.pop()
                ped_looking_history.insert(0, pedActrions[0])
                ped_crossing_history.insert(0, pedActrions[1])
                self.meanDistanceFromMe[objectID].insert(0, distances[0])
                self.meanDistanceFromRoad[objectID].insert(0, distances[1])
                self.meanDistanceFromCar[objectID].insert(0, distances[2])
                self.removeAfterFrames(objectID, 5)
                X_test = [ int(distances[0]), int(distances[1]),
                                         self.read_from_xml(vehicle_annotation, step),
                                         self.calculateLastPedestriansActions(ped_looking_history),
                                         self.calculateLastPedestriansActions(ped_crossing_history),
                                         int(distances[2]),rect[0], rect[3], rect[2]]
                from transform_dataset import importdata
                X_test = np.array(importdata(X_test)).reshape(1,9)
                # with open('../train/models/dataset.csv', 'a', newline='') as file:
                #     writer = csv.writer(file)
                #     writer.writerow(X_test)
                y_pred = clf_loaded.predict(X_test)[0]
                if maxDangerGini < int(y_pred):
                    maxDangerGini = int(y_pred)
            with open('pedestrian.csv', 'r') as file:
                x = int(file.read()[step * 2])
                print(x)
            if x == 1:
                if (maxDangerGini == 1):
                    lowCorrect += 1
                if (maxDangerGini == 2):
                    lowMedium += 1
                if (maxDangerGini == 3):
                    lowHigh += 1
            if x == 2:
                if (maxDangerGini == 1):
                    mediumLow += 1
                if (maxDangerGini == 2):
                    mediumCorrect += 1
                if (maxDangerGini == 3):
                    mediumHigh += 1
            if x == 3:
                if (maxDangerGini == 1):
                    highLow += 1
                if (maxDangerGini == 2):
                    highMedium += 1
                if (maxDangerGini == 3):
                    highCorrect += 1
            vehicleSpeed = self.read_from_xml(vehicle_annotation, step)
            cv2.namedWindow('image', cv2.WINDOW_NORMAL)
            cv2.setMouseCallback('image', self.click_event)
            self.draw_text(image_road_contour, "Danger:"+self.calculateDanger(maxDangerGini))
            # self.draw_text(image_road_contour, "Vehicle speed:"+vehicleSpeed, pos=(0,140))

            img_array.append(image_road_contour)
            cv2.imshow("image", image_road_contour)
            cv2.waitKey(10)


        out = cv2.VideoWriter('video_final.avi',cv2.VideoWriter_fourcc(*'DIVX'),15,(1920,1080))
        import pandas as pd
        data = pd.read_csv("../eval/pedestrian.csv")
        y=data.iloc[:]
        data = pd.read_csv("../train/models/dataset.csv")
        x_val = data.iloc[:]

        def Average(lst):
            return sum(lst) / len(lst)
        cm = [[lowCorrect,lowMedium,lowHigh],[mediumLow,mediumCorrect+1,mediumHigh],[highLow,highMedium,highCorrect]]
        from operator import truediv
        import numpy as np
        print (cm)
        tp = np.diag(cm)
        prec = list(map(truediv, tp, np.sum(cm, axis=0)))
        rec = list(map(truediv, tp, np.sum(cm, axis=1)))
        print('Accuracy:{}'.format((lowCorrect+mediumCorrect+highCorrect)/140))
        print('Precision: {}\nRecall: {}'.format(Average(prec), Average(rec)))
        print((2 * Average(prec) * Average(rec)) / (Average(prec) + Average(rec)))
        for i in range(len(img_array)):
            out.write(img_array[i])
        out.release()
    def calculateLastPedestriansActions(self, history):
        action = False
        for i in history:
            action = i or action
        return action
    def calculateDanger(self, danger):
        if danger == 1:
            return 'Low'
        if danger == 2:
            return 'Medium'
        return 'High'
    def draw_text(self,img, text,
                  font=cv2.FONT_HERSHEY_PLAIN,
                  pos=(3, 3),
                  font_scale=3,
                  font_thickness=2,
                  text_color=(116,116,116),
                  text_color_bg=(255,255,255)
                  ):
        x, y = pos
        text_size, _ = cv2.getTextSize(text, font, font_scale, font_thickness)
        text_w, text_h = text_size
        cv2.rectangle(img, pos, (x + text_w, y + text_h), text_color_bg, -1)
        cv2.putText(img, text, (x, y + text_h + font_scale - 1), font, font_scale, text_color, font_thickness)
        return text_size
    @staticmethod
    def calculateDistanceBetweenFrames(lastDistances, currentDistance, frames=5):
        meanDistance = 0

        if currentDistance == -1:
            currentDistance = [dst for dst in lastDistances if dst > 0]
            currentDistance = currentDistance[0] if len(currentDistance) else 0

        for i, dst in enumerate(lastDistances):
            if i == frames:
                return meanDistance / frames - currentDistance
            if dst > 0:
                meanDistance += dst
        return 0

    def removeAfterFrames(self, objectId, frames):
        if np.array_equal(self.meanDistanceFromRoad[objectId][:frames], np.full((1, frames), -1, dtype=int)):
            self.meanDistanceFromRoad[objectId] = []
        if np.array_equal(self.meanDistanceFromCar[objectId][:frames], np.full((1, frames), -1, dtype=int)):
            self.meanDistanceFromCar[objectId] = []
        if np.array_equal(self.meanDistanceFromMe[objectId][:frames], np.full((1, frames), -1, dtype=int)):
            self.meanDistanceFromMe[objectId] = []