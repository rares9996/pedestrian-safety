from os import listdir
from os.path import isfile, join
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import xml.etree.ElementTree as ET
my_path = "C:\\Users\\Rares\\Desktop\\Licenta\\20220606_1644\\Annotations"
onlyfiles = [f for f in listdir(my_path) if isfile(join(my_path, f))]
annotations = Element('annotations')
track = [None] * 100
for i in range(0,100):
    track[i]=SubElement(annotations,'track',{'label':"pedestrian"})

for frame, file in enumerate(onlyfiles):
    tree = ET.parse(my_path+'\\'+file)
    index=-1;
    for object in tree.findall('object'):
        if object.find("name").text == 'Pedestrian':
            index+=1;
            box = SubElement(track[index],'box', {'frame':str(frame+1),
                                                           'xbr':object.find('bndbox').find('xmax').text,
                                                            'xtl':object.find('bndbox').find('xmin').text,
                                                            'ybr':object.find('bndbox').find('ymax').text,
                                                            'ytl':object.find('bndbox').find('ymin').text})
            for attribute in object.find('attributes').findall('attribute'):
                if(attribute.find('name').text == 'Looking'):
                    if(attribute.find('value').text=='0'):
                        looking = 'not-looking'
                    else:
                        looking = 'looking'
                    SubElement(box,'attribute', {'look': looking})
                if(attribute.find('name').text == 'Cross'):
                    if (attribute.find('value').text == 'False'):
                        crossing = 'not-crossing'
                    else:
                        crossing = 'crossing'
                    SubElement(box, 'attribute', {'cross': crossing})
for tracks in track:
    if tracks == None:
        annotations.remove(tracks)
print (tostring(annotations))

