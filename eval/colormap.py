import numpy as np


def colormap_cityscapes(n):
    cmap = np.zeros([n, 3]).astype(np.uint8)
    cmap[0, :] = np.array([128, 64, 128])  # sosea
    cmap[1, :] = np.array([244, 35, 232])  # trotuar
    cmap[2, :] = np.array([70, 70, 70])  # cladiri
    cmap[3, :] = np.array([102, 102, 156])  # panou publicitar/gard
    cmap[4, :] = np.array([190, 153, 153])
    cmap[5, :] = np.array([153, 153, 153])  # stalp de la semne de circulatie

    cmap[6, :] = np.array([250, 170, 30])  # semafor
    cmap[7, :] = np.array([220, 220, 0])  # semn de circulatie
    cmap[8, :] = np.array([107, 142, 35])  # copaci/verdeata
    cmap[9, :] = np.array([152, 251, 152])  # borduri pe langa copaci
    cmap[10, :] = np.array([70, 130, 180])

    cmap[11, :] = np.array([220, 20, 60])  # pieton simplu
    cmap[12, :] = np.array([255, 0, 0])  # pieton pe bicicleta
    cmap[13, :] = np.array([0, 0, 142])  # masina
    cmap[14, :] = np.array([0, 0, 70])
    cmap[15, :] = np.array([0, 60, 100])

    cmap[16, :] = np.array([0, 80, 100])
    cmap[17, :] = np.array([0, 0, 230])  # motocicleta
    cmap[18, :] = np.array([119, 11, 32])  # bicicleta
    cmap[19, :] = np.array([0, 0, 0])  # obiecte neidenttificate

    return cmap
