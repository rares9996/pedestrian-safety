import numpy as np
import cv2
import os

from PIL import Image

from torch.utils.data import Dataset

EXTENSIONS = ['.jpg', '.png']

class cityscapes(Dataset):

    def __init__(self, root, input_transform=None):
        self.video_name = os.path.join(root)
        self.images = []
        self.input_transform = input_transform
        self.height = 0
        self.width = 0

        print('Read a new frame: ', self.video_name)

        vidcap = cv2.VideoCapture(self.video_name)
        success, image = vidcap.read()
        self.height = len(image)
        self.width = len(image[0])
        while success:
            self.images.append(image)
            success, image = vidcap.read()

    def __getitem__(self, index):

        image = Image.fromarray(np.uint8(self.images[index])).convert('RGB')

        if self.input_transform is not None:
            image = self.input_transform(image)

        return image, self.images[index], self.height, self.width

    def __len__(self):
        return len(self.images)

