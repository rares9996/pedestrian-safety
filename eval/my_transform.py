import math
import torch
import cv2
import numpy as np
from PIL import Image

from colormap import colormap_cityscapes

font = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 0.3
fontColor = (255, 255, 255)
thickness = 1
lineType = 2

from PIL import Image
from torchvision.transforms import Compose, Resize
from torchvision.transforms import ToTensor
import torchvision.transforms as T


class Colorize:

    def __init__(self, n=22):
        self.cmap = colormap_cityscapes(256)
        self.cmap[n] = self.cmap[-1]
        self.cmap = torch.from_numpy(self.cmap[:n])

    @staticmethod
    def euclidianDistance(point1, point2):
        return round(((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2) ** 0.5, 2)
    @staticmethod
    def centerToTopLeft(x, y, w, h):
        x = x - w / 2
        y = y - h / 2
        return x, y, w, h
    @staticmethod
    def distance(x_max, y_max, x_min, y_min, px, py):
      dx = max(x_min - px, 0, px - x_max)
      dy = max(y_min - py, 0, py - y_max)
      return math.sqrt(dx*dx + dy*dy)

    def readFromFile(self,file_path, image_x ,image_y):
        file = open(file_path, "r")
        cars = []
        pedestrian = []
        for line in file:
            cls, x, y, w, h, conf = line.split()
            x, y, w, h = self.centerToTopLeft(float(x),float(y),float(w),float(h))
            if cls == '0':
                pedestrian.append((x*image_x, y*image_y, w*image_x, h*image_y, float(conf)))
            else:
                cars.append((x*image_x, y*image_y, w*image_x, h*image_y, float(conf)))
        return pedestrian, cars
    def calculate_contour(self, label, gray_image, yolop, height, width):
        transform = T.ToPILImage()
        input_transform_cityscapes = Compose([
            Resize((height, width), Image.BILINEAR),
            ToTensor(),
        ])
        image = input_transform_cityscapes(transform(gray_image))
        if yolop:
            mask = image[0] != label
        else:
            mask = image[0] == label

        street_color = torch.ByteTensor(height, width).fill_(0)
        street_color[mask] = self.cmap[label][0]
        img = street_color.numpy()
        img = cv2.dilate(img, np.ones((5,5), np.uint8), iterations=3)
        contour_street, _ = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        return contour_street, street_color
    def __call__(self, gray_image, file_number,height, width, image_raw, yolop=False):
        distance = [[], [], [], []]
        contour_street, street_color = self.calculate_contour(0, gray_image, yolop, height, width)
        road_contour = [street for street in contour_street if cv2.contourArea(street) > 10000]
        image = street_color.numpy()

        bbox_pedestrian, bbox_cars = self.readFromFile("..\\runs\\labels\\"+str(file_number+1)+".txt",width.item(),height.item())
        for i in range(len(bbox_pedestrian)):
            cont = bbox_pedestrian[i]
            extreme_bot = [int(cont[0]+cont[2]),int(cont[1]+cont[3])]
            extreme_bot_left = [int(cont[0]),int(cont[1]+cont[3])]
            center = (int(cont[0]+cont[2]/2),int(cont[1]+cont[3]/2))
            cv2.circle(image, (int(extreme_bot[0]), int(extreme_bot[1])), 10, 100, -1)

            distance_from_me = height.item() - extreme_bot[1]
            distance_to_road = 9999999
            distance_to_car = 9999999
            for y in range(len(bbox_cars)):
                car = bbox_cars[y]
                dst = self.distance(car[0], car[1], car[0] + car[2],
                                    car[1] + car[3], center[0], center[1])
                if (distance_to_car > dst):
                    distance_to_car = dst
            for j in range(len(road_contour)):
                if(cv2.pointPolygonTest(road_contour[j], extreme_bot, False) >= 0 or
                cv2.pointPolygonTest(road_contour[j], extreme_bot_left, False) >= 0):
                    dst = 0
                else:
                    dst = min(abs(cv2.pointPolygonTest(road_contour[j], extreme_bot, True)),
                              abs(cv2.pointPolygonTest(road_contour[j], extreme_bot_left, True)),)
                if(distance_to_road > dst):
                    distance_to_road = dst
            distance[0].extend([distance_from_me,0,0,0])
            distance[1].extend([round(distance_to_road, 5) if distance_to_road < 9999999 else -1, 0, 0, 0])
            distance[2].extend([round(distance_to_car, 5) if distance_to_car < 9999999 else -1, 0, 0, 0])
            distance[3].extend(cont[:4])

        img = image_raw.copy()
        alpha = 0.4
        cv2.drawContours(img, road_contour,  0, (0,255,0), -1)
        result = cv2.addWeighted(image_raw, 1 - alpha, img, alpha, 0)

        return distance, result, bbox_cars
